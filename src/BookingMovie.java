import java.util.Scanner;

public class BookingMovie {
    static Scanner kb = new Scanner(System.in);
    static String Movie[] = { "The Meg 2", "Mission Impossible 7", "The Avenger 5", "Harry Potter", "Oppenheimer" };
    static int price = 220;
    static String day = "Sun ";
    static String theater[] = { "1", "2", "3", "4", "5" };
    static String seat[][] = { { "F1", "F2", "F3", "F4", "F5", "F6", "F7" },
            { "E1", "E2", "E3", "E4", "E5", "E6", "E7" },
            { "D1", "D2", "D3", "D4", "D5", "D6", "D7" },
            { "C1", "C2", "C3", "C4", "C5", "C6", "C7" },
            { "B1", "B2", "B3", "B4", "B5", "B6", "B7" },
            { "A1", "A2", "A3", "A4", "A5", "A6", "A7" } };

    static String showtime[] = { "11.00", "13.40", "16.20", "19.00", "21.40" };
    static int Num;
    static String[] Select;
    static String selectedMovie;
    static String Time;

    public static void printSeat() {
        System.out.println("--------------------");
        System.out.println("       Display      ");
        System.out.println();
        for (int i = 0; i < 6; i++) {
            for (int j = 0; j < 7; j++) {
                System.out.print(seat[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println("--------------------");
    }

    public static void inputSeat() {
        while (true) {
            System.out.print("How Many Ticket Do You Want?: ");
            Num = kb.nextInt();
            Select = new String[Num];
            if (Num > 0) {
                for (int i = 0; i < Num; i++) {
                    boolean validSeat = false;
                    while (!validSeat) {
                        System.out.print("What Seat "+ (i+1) +" Do You Want?: ");
                        String selected = kb.next();
                        if (isValidSeat(selected)) {
                            Select[i] = selected;
                            seatSearch(selected);
                            validSeat = true;
                        } else {
                            System.out.println("No seats are available.");
                        }
                    }
                }
                printSeat();
                movieConfirm();
                break;
            } else {
                System.out.println("Error Try Again!!");
            }
        }
    }

    public static void seatSearch(String Selected) {
        for (int i = 0; i < 6; i++) {
            for (int j = 0; j < 7; j++) {
                if (Selected.equals(seat[i][j])) {
                    seat[i][j] = "--";
                }
            }
        }
    }

    public static void showMovie() {
        System.out.println("--------------------");
        System.out.println("  Movie List Today");
        for (int i = 0; i < Movie.length; i++) {
            System.out.println(i + 1 + "." + Movie[i]);
        }
        System.out.println("--------------------");
        movieSelect();
    }

    public static boolean isValidSeat(String seatNumber) {
        for (int i = 0; i < 6; i++) {
            for (int j = 0; j < 7; j++) {
                if (seatNumber.equals(seat[i][j])) {
                    return true;
                }
            }
        }
        return false;
    }

    public static void movieSelect() {
            while (true) {
                System.out.print("Select Your Movie : ");
                int movieSelect = kb.nextInt();
                if (movieSelect >= 1 && movieSelect <= Movie.length) {
                    selectedMovie = Movie[movieSelect - 1];
                    System.out.println("--------------------");
                    System.out.println("Movie: [ " + selectedMovie + " ]");
                    movieTime();
                    break;
                } else {
                    System.out.println("Error Try Again!!");
                }
            }
        }

    public static void movieTime() {
        System.out.println("--------------------");
        System.out.println("Select Your ShowTime");
        for (int i = 0; i < showtime.length; i++) {
            System.out.println(i + 1 + "." + showtime[i]);
        }
        System.out.println("--------------------");
        movieTimeSelect();
    }

    public static void movieTimeSelect() {
            while (true) {
                System.out.print("Select Your Time : ");
                int timeSelect = kb.nextInt();
                if (timeSelect >= 1 && timeSelect <= showtime.length) {
                    Time = showtime[timeSelect - 1];
                    System.out.println("--------------------");
                    System.out.println("Time: [ " + Time + " ]");
                    printSeat();
                    break;
                } else {
                    System.out.println("Error Try Again!!");
                }
            }
        }

    static void movieConfirm() {
        System.out.println("       Bills        ");
        System.out.println("--------------------");
        System.out.println("Movie : " + selectedMovie);
        System.out.println("Showtime : " + day + Time);
        for (int i = 0; i < Select.length; i++) {
            System.out.println("Seat : " + Select[i] + " " + price + " Baht");
        }
        System.out.println("Total : " + price * Num + " Baht");
        System.out.println("--------------------");
        System.out.println("Confirm Your Bills (y/n)");
        char Ans = kb.next().charAt(0);
        if (Ans == 'y' || Ans == 'Y') {
            printTicket();
        } else if (Ans == 'n' || Ans == 'N') {
            System.out.println("Your bill has been canceled.");
        }
    }

    static void printTicket() {
        for (int i = 0; i < Num; i++) {
            System.out.println("--------------------");
            System.out.println("       Ticket       ");
            System.out.println("--------------------");
            System.out.println("   Movie : " + selectedMovie);
            Showtheater();
            System.out.println("Showtime : " + day + Time);
            System.out.println("   Seat  : " + Select[i]);
        }
        System.out.println("--------------------");
        System.out.println("--------------------");
    }

    private static void Showtheater() {
        for (int i = 0; i < Movie.length; i++) {
            if (selectedMovie.equals(Movie[i])) {
                System.out.println(" Theater : " + theater[i]);
            }
        }
    }

    public static void main(String[] args) {
        System.out.println("     CS Cinema     ");
        showMovie();
        inputSeat();
    }
}